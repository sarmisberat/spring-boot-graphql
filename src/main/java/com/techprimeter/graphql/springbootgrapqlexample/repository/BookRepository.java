package com.techprimeter.graphql.springbootgrapqlexample.repository;

import com.techprimeter.graphql.springbootgrapqlexample.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book, String> {

}
